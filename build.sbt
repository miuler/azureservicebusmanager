name := "backend"
version := "1.0.0"
scalaVersion := "2.13.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.0-M1" % Test
libraryDependencies += "com.typesafe" % "config" % "1.4.0"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

libraryDependencies += "com.microsoft.azure" % "azure-servicebus" % "3.1.4"

libraryDependencies += "org.openjfx" % "javafx-base" % "11.0.2" classifier "linux"
libraryDependencies += "org.openjfx" % "javafx-controls" % "11.0.2" classifier "linux"
libraryDependencies += "org.openjfx" % "javafx-graphics" % "11.0.2" classifier "linux"
libraryDependencies += "org.openjfx" % "javafx-fxml" % "11.0.2" classifier "linux"

libraryDependencies += "org.openjfx" % "javafx-base" % "11.0.2"
libraryDependencies += "org.openjfx" % "javafx-controls" % "11.0.2"
libraryDependencies += "org.openjfx" % "javafx-graphics" % "11.0.2"
libraryDependencies += "org.openjfx" % "javafx-fxml" % "11.0.2"

