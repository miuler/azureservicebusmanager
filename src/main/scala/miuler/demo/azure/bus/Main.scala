package miuler.demo.azure.bus

import com.microsoft.azure.servicebus.TopicClient
import com.microsoft.azure.servicebus.management.ManagementClient
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder
import com.typesafe.scalalogging.LazyLogging

object Main extends LazyLogging {
//  Logger.setDefaultFormatter(SourceCodeLogFormatter)
//  Logger.setDefaultFormatter(IntelliJLogFormatter)
  logger.info("Inicio de aplicacion")

  val connectionStringBuilder = new ConnectionStringBuilder("xxx")
  val managementClient = new ManagementClient(connectionStringBuilder)
  val topicClient = new TopicClient(connectionStringBuilder)
//  val subscriptionClient = new SubscriptionClient(connectionStringBuilder)

  managementClient.getTopics.forEach(topicDescription => {
    logger.info("topicDescription: " + topicDescription)
    logger.info("    entityStatus: " + topicDescription.getEntityStatus)
    logger.info("    path: " + topicDescription.getPath)
  })

  val reintento = managementClient.getTopicRuntimeInfo("reintento")
  logger.info("reintento: " + reintento)
  logger.info("reintento.getSizeInBytes: " + reintento.getSizeInBytes)
  logger.info("reintento.getSubscriptionCount: " + reintento.getSubscriptionCount)
  logger.info("reintento.getMessageCountDetails: " + reintento.getMessageCountDetails)
  logger.info("reintento.getMessageCountDetails.getActiveMessageCount: " + reintento.getMessageCountDetails.getActiveMessageCount)
  logger.info("reintento.getMessageCountDetails.getDeadLetterMessageCount: " + reintento.getMessageCountDetails.getDeadLetterMessageCount)
  logger.info("reintento.getMessageCountDetails.getScheduledMessageCount: " + reintento.getMessageCountDetails.getScheduledMessageCount)

  managementClient.close()
}
