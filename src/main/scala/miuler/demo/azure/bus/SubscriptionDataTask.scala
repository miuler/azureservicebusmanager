package miuler.demo.azure.bus

import com.microsoft.azure.servicebus.management.ManagementClient
import com.typesafe.scalalogging.LazyLogging
import javafx.beans.property.{SimpleIntegerProperty, SimpleLongProperty, SimpleStringProperty}
import javafx.collections.ObservableList
import javafx.concurrent.Task

class SubscriptionDataTask(managementClientOption: Option[ManagementClient],
                           topic: String) extends Task[List[SubscriptionInfo]] with LazyLogging {

  //  val topicInfos = new util.ArrayList[TopicInfo]()
  val subscriptionInfoList = scala.collection.mutable.ArrayBuffer.empty[SubscriptionInfo]

  override def call(): List[SubscriptionInfo] = {
    var i = 2

    managementClientOption.foreach(managementClient => managementClient.getSubscriptions(topic).forEach(subscriptionDescription => {
      logger.info("subscriptionDescription: " + subscriptionDescription)
      logger.info("subscriptionDescription.path: " + subscriptionDescription.getPath)
      logger.info("subscriptionDescription.topicPath: " + subscriptionDescription.getTopicPath)
      logger.info("subscriptionDescription.subscriptionName: " + subscriptionDescription.getSubscriptionName)
      val subscriptionRuntimeInfo = managementClient.getSubscriptionRuntimeInfo(subscriptionDescription.getTopicPath, subscriptionDescription.getSubscriptionName)
      logger.info("subscriptionRuntimeInfo.messageCount: " + subscriptionRuntimeInfo.getMessageCount)
      logger.info("subscriptionRuntimeInfo.messageCountDetails: " + subscriptionRuntimeInfo.getMessageCountDetails)

      val subscriptionInfo = new SubscriptionInfo(
        new SimpleStringProperty(subscriptionDescription.getPath),
        new SimpleStringProperty(subscriptionDescription.getSubscriptionName),
        new SimpleLongProperty(subscriptionRuntimeInfo.getMessageCount))

      subscriptionInfoList.append(subscriptionInfo)
      i += 1
    }))

    subscriptionInfoList.toList
  }

}


