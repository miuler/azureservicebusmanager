package miuler.demo.azure.bus

import com.microsoft.azure.servicebus.management.ManagementClient
import com.typesafe.scalalogging.LazyLogging
import javafx.collections.ObservableList
import javafx.concurrent.Task
import miuler.demo.azure.bus.TopicInfo._

class TopicDataTask(managementClientOption: Option[ManagementClient]) extends Task[List[TopicInfo]] with LazyLogging {

  logger.debug("Constructor")
//  val topicInfos = new util.ArrayList[TopicInfo]()
  val topicInfos = scala.collection.mutable.ArrayBuffer.empty[TopicInfo]

  override def call(): List[TopicInfo] = {
    logger.debug("call")
    var i = 2
    managementClientOption.foreach(managementClient => {
      logger.info("managementClient: " + managementClient)
      val topics = managementClient.getTopics()
      logger.info(s"topics.size: ${topics.size()}")
      topics.forEach(topicDescription => {
        logger.info("topicDescription: " + topicDescription)

        val topicRuntimeInfo = managementClient.getTopicRuntimeInfo(topicDescription.getPath)
        val descriptionTopic =
          s"""path: ${topicDescription.getPath} (${topicDescription.getEntityStatus})
             |    - subscriptionCount: ${topicRuntimeInfo.getSubscriptionCount}
             |    - sizeInBytes: ${topicRuntimeInfo.getSizeInBytes}
             |    - messageCountDetails.activeMessageCount: ${topicRuntimeInfo.getMessageCountDetails.getActiveMessageCount}
             |    - messageCountDetails.deadLetterMessageCount: ${topicRuntimeInfo.getMessageCountDetails.getDeadLetterMessageCount}
             |    - messageCountDetails.scheduledMessageCount: ${topicRuntimeInfo.getMessageCountDetails.getScheduledMessageCount}
         """.stripMargin
        logger.info(descriptionTopic)

        val topicInfo = new TopicInfo(
          topicDescription.getPath,
          topicDescription.getEntityStatus,
          topicRuntimeInfo.getSubscriptionCount,
          topicRuntimeInfo.getSizeInBytes,
          Utils.bytes2String(topicRuntimeInfo.getSizeInBytes),
          topicRuntimeInfo.getMessageCountDetails.getActiveMessageCount,
          topicRuntimeInfo.getMessageCountDetails.getDeadLetterMessageCount,
          topicRuntimeInfo.getMessageCountDetails.getScheduledMessageCount
        )
        //      topicInfos.add(topicInfo)
        topicInfos.append(topicInfo)
        i += 1
      })
    })
    logger.debug("end getTopics")
    topicInfos.toList
  }
}


