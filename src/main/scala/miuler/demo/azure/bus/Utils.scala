package miuler.demo.azure.bus

import javafx.beans.property.SimpleStringProperty

object Utils {

  import java.text.DecimalFormat
  import java.text.NumberFormat

  val SPACE_KB = 1024
  val SPACE_MB: Double = 1024 * SPACE_KB
  val SPACE_GB: Double = 1024 * SPACE_MB
  val SPACE_TB: Double = 1024 * SPACE_GB

  def bytes2String(sizeInBytes: Long): String = {
    val nf = new DecimalFormat
    nf.setMaximumFractionDigits(2)
    try
        if (sizeInBytes < SPACE_KB) nf.format(sizeInBytes) + " Byte(s)"
        else if (sizeInBytes < SPACE_MB) nf.format(sizeInBytes / SPACE_KB) + " KB"
        else if (sizeInBytes < SPACE_GB) nf.format(sizeInBytes / SPACE_MB) + " MB"
        else if (sizeInBytes < SPACE_TB) nf.format(sizeInBytes / SPACE_GB) + " GB"
        else nf.format(sizeInBytes / SPACE_TB) + " TB"
    catch {
      case e: Exception =>
        sizeInBytes + " Byte(s)"
    }
    /* end of bytes2String method */
  }

}
