package miuler.demo.azure.bus

import javafx.beans.property.{SimpleLongProperty, SimpleStringProperty}

class SubscriptionInfo(path: SimpleStringProperty, subscriptionName: SimpleStringProperty, messageCount: SimpleLongProperty) {
  def getPath() = path.get()
  def getSubscriptionName() = subscriptionName.get()
  def getMessageCount() = messageCount.get()
}
