package miuler.demo.azure.bus

import com.microsoft.azure.servicebus.management.EntityStatus
import javafx.beans.property.{SimpleIntegerProperty, SimpleLongProperty, SimpleStringProperty}

import scala.beans.BeanProperty


case class TopicInfo(@BeanProperty var path: String,
                     @BeanProperty var status: String,
                     @BeanProperty var subscriptionCount: Int,
                     @BeanProperty var sizeInBytes: Long,
                     @BeanProperty var size: String,
                     @BeanProperty var activeMessageCount: Long,
                     @BeanProperty var deadLetterMessageCount: Long,
                     @BeanProperty var scheduledMessageCount: Long)


object TopicInfo {
  implicit def SimpleStringProperty2EntityStatus(simpleStringProperty: SimpleStringProperty) = EntityStatus.valueOf(simpleStringProperty.get)
  implicit def SimpleStringProperty2String(simpleStringProperty: SimpleStringProperty) = simpleStringProperty.get
  implicit def SimpleIntegerProperty2Int(simpleIntegerProperty: SimpleIntegerProperty) = simpleIntegerProperty.get
  implicit def SimpleLongProperty2Long(simpleLongProperty: SimpleLongProperty) = simpleLongProperty.get
  implicit def EntityStatus2SimpleStringProperty(entityStatus: EntityStatus) = new SimpleStringProperty(entityStatus.toString)
  implicit def String2SimpleStringProperty(string: String) = new SimpleStringProperty(string)
  implicit def Int2SimpleIntegerProperty(int: Int) = new SimpleIntegerProperty(int)
  implicit def Long2SimpleLongProperty(long: Long) = new SimpleLongProperty(long)
  implicit def EntityStatus2String(entityStatus: EntityStatus) = entityStatus.toString
}