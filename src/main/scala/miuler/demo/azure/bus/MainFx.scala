package miuler.demo.azure.bus

import java.util
import java.util.concurrent.Executors

import com.microsoft.azure.servicebus.management.{ManagementClient, ManagementClientAsync}
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder
import com.microsoft.azure.servicebus.{SubscriptionClient, TopicClient}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.LazyLogging
import javafx.application.Application
import javafx.collections.{FXCollections, ObservableList}
import javafx.event.ActionEvent
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control._
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.MouseButton
import javafx.scene.layout.{StackPane, VBox}
import javafx.stage.Stage


class AppFx extends Application with LazyLogging {
  val config = ConfigFactory.load()
  val executorService = Executors.newFixedThreadPool(50)

  val javaVersion = System.getProperty("java.version")
  val javafxVersion = System.getProperty("javafx.version")

  var connectionStringBuilder: ConnectionStringBuilder = _
  var managementClientOption: Option[ManagementClient] = None
  var topicClient: Option[TopicClient] = None
  var subscriptionClient: Option[SubscriptionClient] = None
//  val subscriptionTableView = initSubscriptionTableView()
  val topicInfodatas = FXCollections.observableArrayList(new util.ArrayList[TopicInfo]())
  val topicsTableView = initTopicsTableView(topicInfodatas)
  val stackPane = new StackPane()
  val mainVBox = new VBox()


  override def init(): Unit = {
    logger.info("init ----------------------------")
    logger.info("javaVersion: " + javaVersion)
    logger.info("javafxVersion: " + javafxVersion)

    val namespace = config.getString("bus.namespace")
    val primaryKey = config.getString("bus.sas-key")
    val sasPolicyName = config.getString("bus.policyname")
    val connectionString = s"Endpoint=sb://$namespace.servicebus.windows.net/;SharedAccessKeyName=$sasPolicyName;SharedAccessKey=$primaryKey"
    logger.info(s"connectionString: $connectionString")

    connectionStringBuilder = new ConnectionStringBuilder(connectionString)
    managementClientOption = Some(new ManagementClient(connectionStringBuilder))
    logger.debug(s"managementClientOption: $managementClientOption")
  }


  private def initTopicsTableView(topicInfodatas: ObservableList[TopicInfo]): TableView[TopicInfo] = {
    val pathColumn = new TableColumn[TopicInfo, String]("Path")
    pathColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("path"))

    val statusColumn = new TableColumn[TopicInfo, String]("Status")
    statusColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("status"))

    val subscriptionCountColumn = new TableColumn[TopicInfo, String]("SubscriptionCount")
    subscriptionCountColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("subscriptionCount"))

    val sizeInBytesColumn = new TableColumn[TopicInfo, String]("SizeInBytes")
    sizeInBytesColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("sizeInBytes"))

    val sizeColumn = new TableColumn[TopicInfo, String]("Size")
    sizeColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("size"))

    val activeMessageCountColumn = new TableColumn[TopicInfo, String]("ActiveMessageCount")
    activeMessageCountColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("activeMessageCount"))

    val deadLetterMessageCountColumn = new TableColumn[TopicInfo, String]("DeadLetterMessageCount")
    deadLetterMessageCountColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("deadLetterMessageCount"))

    val scheduledMessageCountColumn = new TableColumn[TopicInfo, String]("ScheduledMessageCount")
    scheduledMessageCountColumn.setCellValueFactory(new PropertyValueFactory[TopicInfo, String]("scheduledMessageCount"))



    val tableView = new TableView[TopicInfo]()
    tableView.getColumns.addAll(
      pathColumn,
      statusColumn,
      subscriptionCountColumn,
      sizeInBytesColumn,
      sizeColumn,
      activeMessageCountColumn,
      deadLetterMessageCountColumn,
      scheduledMessageCountColumn)
    tableView.setItems(topicInfodatas)
    tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY)

    tableView.setRowFactory(tv => {
      val row = new TableRow[TopicInfo]
      row.setOnMouseClicked(mouseEvent => {
        if (!row.isEmpty &&
          mouseEvent.getButton == MouseButton.PRIMARY &&
          mouseEvent.getClickCount == 2) {

          val topicInfo = row.getItem

          val subscriptionInfoData = FXCollections.observableArrayList(new util.ArrayList[SubscriptionInfo]())
          loadSubscription(subscriptionInfoData, topicInfo.getPath())


          val stage = new Stage()
          val label = new Label(s"topicInfo.path: ${topicInfo.getPath()}")
          val vbox = new VBox()
          vbox.getChildren.add(label)
          vbox.getChildren.add(initSubscriptionTableView(subscriptionInfoData))
          stage.setScene(new Scene(vbox, 300, 300))

          stage.show()
        }
      })
      row
    })


    tableView
  }


  private def loadSubscription(subscriptionInfoData: ObservableList[SubscriptionInfo], topic: String) = {
    logger.info("loadSubscription: =======")
    subscriptionInfoData.clear()
    val subscriptionDataTask = new SubscriptionDataTask(managementClientOption, topic)
    subscriptionDataTask.setOnSucceeded(event => subscriptionInfoData.addAll(subscriptionDataTask.getValue: _*))

    //executorService.execute(subscriptionDataTask)
    executorService.submit(subscriptionDataTask)
  }

  private def initSubscriptionTableView(subscriptionInfoData: ObservableList[SubscriptionInfo]): TableView[SubscriptionInfo] = {
    val pathColumn = new TableColumn[SubscriptionInfo, String]("Path")
    pathColumn.setCellValueFactory(new PropertyValueFactory[SubscriptionInfo, String]("path"))

    val subscriptionNameColumn = new TableColumn[SubscriptionInfo, String]("SubscriptionName")
    subscriptionNameColumn.setCellValueFactory(new PropertyValueFactory[SubscriptionInfo, String]("subscriptionName"))

    val messageCountColumn = new TableColumn[SubscriptionInfo, String]("MessageCount")
    messageCountColumn.setCellValueFactory(new PropertyValueFactory[SubscriptionInfo, String]("messageCount"))


    val tableView = new TableView[SubscriptionInfo]()
    tableView.getColumns.addAll(pathColumn, subscriptionNameColumn, messageCountColumn)
    tableView.setItems(subscriptionInfoData)
    tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY)


    tableView.setRowFactory(tv => {
      val row = new TableRow[SubscriptionInfo]
      row.setOnMouseClicked(mouseEvent => {
//        if (!row.isEmpty &&
//          mouseEvent.getButton == MouseButton.PRIMARY &&
//          mouseEvent.getClickCount == 2) {
//
//          val subscriptionInfo = row.getItem
//
//          val stage = new Stage()
////          val label = new Label(s"topicInfo.path: ${subscriptionClient.get()}")
//          val label = new Label(s"topicInfo.path: ${subscriptionInfo}")
//          val vbox = new VBox()
//          vbox.getChildren.add(label)
//          stage.setScene(new Scene(vbox, 300, 300))
//
//          stage.show()
//        }
      })
      row
    })

    tableView
  }

  override def stop(): Unit = {
    logger.info("stop ----------------------------")
    managementClientOption.foreach(_.close())
    logger.debug("stopped managementClientOption")
    topicClient.foreach(_.close())
    logger.debug("stopped topicClient")
    subscriptionClient.foreach(_.close())
    logger.debug("stopped subscriptionClient")
    executorService.shutdown()
    logger.debug("stopped executorService")
  }

  private def initMainScene(): Scene = {
    //    primaryStage.setTitle("Hello World!")
    val button = new Button("Reload")
    button.setText("RELOAD TOPICS")
    button.setOnAction((e: ActionEvent) => {
      logger.info("RELOAD TOPICS!")
      onReloadTopics(topicInfodatas)
    })

    mainVBox.setSpacing(5)
    mainVBox.setPadding(new Insets(10, 0, 0, 10))
    mainVBox.getChildren.add(new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + "."))
    mainVBox.getChildren.add(button)
    mainVBox.getChildren.add(topicsTableView)

    stackPane.getChildren.add(mainVBox)
    val scene: Scene = new Scene(stackPane, 1500, 700)
    scene
  }

  override def start(primaryStage: Stage): Unit = {
    logger.debug("start ---------------------------")
    onReloadTopics(topicInfodatas)

    primaryStage.setTitle("Prueba con Azure Topic")
    primaryStage.setScene(initMainScene())
    primaryStage.show()
  }

  def onReloadTopics(data: ObservableList[TopicInfo]): Unit = {
    logger.debug("onReloadTopics -------------------")

    mainVBox.setDisable(true)
    val progressIndicator = new ProgressIndicator
    val vbox = new VBox(progressIndicator)
    vbox.setAlignment(javafx.geometry.Pos.CENTER)
    stackPane.getChildren.add(vbox)

    data.clear()
    val dataTask = new TopicDataTask(managementClientOption)
    dataTask.setOnSucceeded(event => {
      logger.debug("OnSucceeded: " + event)
      stackPane.getChildren.remove(vbox)
      mainVBox.setDisable(false)
      logger.debug(s"dataTask.getValue.size: ${dataTask.getValue.size}")
      data.addAll(dataTask.getValue: _*)
    })

    logger.debug("executorService.execute(dataTask) 1")
    executorService.execute(dataTask)
    logger.debug("executorService.execute(dataTask) 2")
  }

}

object AppFx extends App {
  Application.launch(classOf[AppFx])
}


